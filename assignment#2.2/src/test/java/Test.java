import infotreni.TrainInfo;
import infotreni.TrainInfoImpl;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;

public class Test {
    public static void main(String[] args) {
        TrainInfo trainInfo = TrainInfoImpl.getInstance();
        System.out.println("Begin");

        //Future<Buffer> solution = trainInfo.getTrainSolutions("5059", "8409", LocalDate.now(), LocalTime.now());
        Future<Buffer> train = trainInfo.getRealTimeTrainInfo(3920);
        //Future<Buffer> station = trainInfo.getRealTimeStationInfo("S07101");
        System.out.println("End");

        //station.onSuccess(body -> log("solution: " + body));
        train.onSuccess(body -> log("train: " + body.toJsonObject().encodePrettily()));
        /*solution.onSuccess(body -> {
            log("solution: ");
            TrainSolution trainSolution = body.toJsonObject().mapTo(TrainSolution.class);
            System.out.println(trainSolution.toString());
            trainSolution.getSolutions().forEach(s -> {
                System.out.println(s.toString());
            });
        });*/
    }

    private static void log(String msg) {
        System.out.println("[MAIN] " + msg);
    }
}
