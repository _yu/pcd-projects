package infotreni;

import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;

import java.time.LocalDate;
import java.time.LocalTime;

public interface TrainInfo {

    /**
     * Get possible solutions of a travel between
     * a starting station and a destination,
     * in a certain date and from a certain time.
     * @param startId Id of starting station
     * @param destId Id of destination station
     * @param date The date of requested solutions
     * @param time The time from which to start requesting solutions
     * @return Future of the response body, which is in json format
     */
    Future<Buffer> getTrainSolutions(String startId, String destId, LocalDate date, LocalTime time);

    /**
     * Get info on current state of given train.
     * @param trainId Id of the train
     * @return Future of the response body, which is in json format
     */
    Future<Buffer> getRealTimeTrainInfo(int trainId);

    /**
     * Get info on current state of given station.
     * @param stationId Id of the station
     * @return Future of the response body, which is in json format
     */
    Future<Buffer> getRealTimeStationInfo(String stationId);

    /**
     * Get list of stations whose full name starts with given string.
     * @param name The station name to autocomplete
     * @return Future of the response body, which is in json format
     */
    Future<Buffer> getStation(String name);

}
