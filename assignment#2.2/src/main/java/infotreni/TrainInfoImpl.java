package infotreni;

import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

// Singleton pattern
// Chose to use the pattern instead of using static methods
// so as to have the TrainInfo interface for convenience.
public class TrainInfoImpl implements TrainInfo{

    private static TrainInfoImpl instance = new TrainInfoImpl();
    private Vertx vertx;
    private HttpClientOptions options;
    private HttpClient client;
    private boolean printLogs = true;

    private TrainInfoImpl(){
        vertx = Vertx.vertx();
        options = new HttpClientOptions()
                .setKeepAlive(true)
                .setDefaultHost("www.viaggiatreno.it")
                .setDefaultPort(80);
        client = vertx.createHttpClient(options);
    }

    public static TrainInfoImpl getInstance() {
        return instance;
    }

    // send get request
    private void doRequest(Promise<Buffer> promise, String logPrefix, String requestURI){
        client.request(HttpMethod.GET, requestURI)
                .onFailure(e -> log(logPrefix, e.getMessage()))
                .onSuccess(request -> {
                    log(logPrefix, "connected to host");
                    request.send()
                            .onFailure(e -> log(logPrefix, e.getMessage()))
                            .onSuccess(response -> {
                                log(logPrefix, "response: " + response.statusMessage() + " - " + response.statusCode());
                                response.body()
                                        .onSuccess(body -> {
                                            log(logPrefix, "body: " + body);
                                            promise.complete(body);
                                        })
                                        .onFailure(e -> {
                                            log(logPrefix, e.getMessage());
                                            promise.fail(e);
                                        });
                            });
                });
    }

    @Override
    public Future<Buffer> getTrainSolutions(String startId, String destId, LocalDate date, LocalTime time) {
        Promise<Buffer> promise = Promise.promise();
        Future<Buffer> future = promise.future();

        doRequest(promise, "get TRAIN SOLUTIONS",
                "/viaggiatrenonew/resteasy/viaggiatreno/soluzioniViaggioNew/" + startId + "/"
                        + destId + "/" + date.toString() + "T" + time);
        return future;
    }

    @Override
    public Future<Buffer> getRealTimeTrainInfo(int trainId) {

        Promise<Buffer> promise = Promise.promise();
        Future<Buffer> future = promise.future();

        getTrainParams(trainId).onSuccess( trainParams -> {
            JsonObject json = trainParams.toJsonObject();
            String startId = json.getString("codLocOrig"); // codice stazione di partenza del treno
            long currTime = Timestamp.valueOf(LocalDateTime.now()).getTime();

            doRequest(promise, "get TRAIN INFO",
                    "/viaggiatrenomobile/resteasy/viaggiatreno/andamentoTreno/"
                            + startId + "/" + trainId + "/" + currTime);
        });

        return future;
    }

    @Override
    public Future<Buffer> getRealTimeStationInfo(String stationId) {

        Promise<Buffer> promise = Promise.promise();
        Future<Buffer> future = promise.future();

        getRegionCode(stationId).onSuccess( code -> {
            String region = code.toString();

            doRequest(promise, "get STATION INFO",
                    "/viaggiatrenonew/resteasy/viaggiatreno/dettaglioStazione/"
                            + stationId + "/" + region);
        });
        return future;
    }

    @Override
    public Future<Buffer> getStation(String name) {
        Promise<Buffer> promise = Promise.promise();
        Future<Buffer> future = promise.future();

        doRequest(promise, "get STATION AUTOCOMPLETE",
                "/viaggiatrenonew/resteasy/viaggiatreno/autocompletaStazione/" + name);
        return future;
    }

    /**
     * Get generic info on specified train;
     * for example, starting station code is necessary
     * for query to get the current state of a train.
     * @param trainId Id of the train
     * @return Future of the response body, which is in json format
     */
    private Future<Buffer> getTrainParams(int trainId){
        Promise<Buffer> promise = Promise.promise();
        Future<Buffer> future = promise.future();

        doRequest(promise,"get TRAIN PARAMS",
                "/viaggiatrenonew/resteasy/viaggiatreno/cercaNumeroTreno/" + trainId);
        return future;
    }

    /**
     * Get the region code of a station, necessary for
     * the query to retrieve information on a station.
     * @param stationId Id of the station
     * @return Future of the response body, which is a string
     */
    private Future<Buffer> getRegionCode(String stationId){
        Promise<Buffer> promise = Promise.promise();
        Future<Buffer> future = promise.future();

        doRequest(promise, "get REGION CODE",
                "/viaggiatrenonew/resteasy/viaggiatreno/regione/" + stationId);
        return future;
    }

    private void log(String prefix, String msg) {
        if (printLogs) System.out.println("[" + prefix + "] " + msg);
    }

}
