package view;

import infotreni.TrainInfo;
import infotreni.TrainInfoImpl;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.converter.DateTimeStringConverter;
import model.TrainInformation.Train;
import model.TrainInformation.TrainStop;
import model.TrainSolution.Solution;
import model.TrainSolution.TrainSolution;
import model.TrainTimerTask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Timer;


/**
 * The application view.
 * */
public class ViewFX {
    TrainInfo trainInfo = TrainInfoImpl.getInstance();

    Timer requestTimer;
    String departureStation = "";
    String arrivalStation = "";
    String trainId;
    final TextArea generalTextArea = new TextArea();
    final TableView<TrainStop> trainStopTableView = new TableView<>();

    public ViewFX(final Stage stage) {
        stage.setTitle("ERYS Trenitalia");
        final FlowPane root = new FlowPane();

        /*------------------- SET SOLUTION CONTROLS------------------- */

        final FlowPane solutionControl = new FlowPane();
        final Label solutionTitle = new Label("Search Solutions");
        final HBox stationHBox = new HBox(20);
        final HBox dateTimeHBox = new HBox(20);
        final ComboBox<String> departureBox = new ComboBox<>();
        final ComboBox<String> arrivalBox = new ComboBox<>();
        final DatePicker datePicker = new DatePicker();
        final TextField timeField = new TextField();
        final SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        final Button searchButton = new Button("Search");

        final TableView<Solution> solutionTableView = new TableView<Solution>();
        solutionTableView.setEditable(false);
        solutionTableView.setPrefSize(500, 480);

        final TableColumn<Solution, String> column1 = new TableColumn<>("Duration");
        column1.setPrefWidth(70);
        column1.setCellValueFactory(new PropertyValueFactory<>("duration"));

        final TableColumn<Solution, String> column2 = new TableColumn<>("Departure");
        column2.setPrefWidth(70);
        column2.setCellValueFactory(new PropertyValueFactory<>("departureTime"));

        final TableColumn<Solution, String> column3 = new TableColumn<>("Arrival");
        column3.setPrefWidth(70);
        column3.setCellValueFactory(new PropertyValueFactory<>("arrivalTime"));

        final TableColumn<Solution, String> column4 = new TableColumn<>("Trains");
        column4.setPrefWidth(135);
        column4.setCellValueFactory(new PropertyValueFactory<>("trains"));

        final TableColumn<Solution, String> column5 = new TableColumn<>("Changes");
        column5.setPrefWidth(151);
        column5.setCellValueFactory(new PropertyValueFactory<>("trainChanges"));

        solutionTableView.getColumns().addAll(column1, column2, column3, column4, column5);

        solutionControl.setAlignment(Pos.CENTER);
        stationHBox.setPadding(new Insets(10));
        dateTimeHBox.setPadding(new Insets(10));

        departureBox.setPromptText("Departure station");
        departureBox.setEditable(true);
        departureBox.setPrefWidth(250);
        this.manageStationResponse(departureBox);

        arrivalBox.setPromptText("Arrival station");
        arrivalBox.setEditable(true);
        arrivalBox.setPrefWidth(250);
        this.manageStationResponse(arrivalBox);

        datePicker.setValue(LocalDate.now());
        datePicker.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) < 0 );
            }
        });

        timeField.setPrefWidth(50);
        try {
            timeField.setTextFormatter(new TextFormatter<>(new DateTimeStringConverter(format), format.parse(LocalTime.now().toString())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.addSearchButtonListener(searchButton, departureBox, arrivalBox, datePicker, timeField, solutionTableView);
        stationHBox.getChildren().addAll(departureBox, arrivalBox);
        dateTimeHBox.getChildren().addAll(datePicker, timeField, searchButton);
        solutionControl.getChildren().addAll(solutionTitle, stationHBox, dateTimeHBox, solutionTableView);

        /*------------------- TRAIN MONITORING CONTROLS------------------- */

        final FlowPane monitoringControl = new FlowPane();
        final HBox trainIdHBox = new HBox(20);
        final HBox controlsHBox = new HBox(20);
        final Label trainIdLabel = new Label("Insert train ID you want to monitor:");
        final TextField trainIdField = new TextField();
        final Button startButton = new Button("Start");
        final Button stopButton = new Button("Stop");

        final VBox infoHBox = new VBox(20);

        stopButton.setDisable(true);
        infoHBox.setAlignment(Pos.CENTER);
        generalTextArea.setEditable(false);
        generalTextArea.setWrapText(true);
        generalTextArea.setPrefSize(500, 80);

        trainStopTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        trainStopTableView.setEditable(false);
        trainStopTableView.setPrefSize(500, 420);

        final TableColumn<TrainStop, String> column6 = new TableColumn<>("Station");
        column6.setCellValueFactory(new PropertyValueFactory<>("station"));

        final TableColumn<TrainStop, String> column7 = new TableColumn<>("Arrival");
        column7.setCellValueFactory(new PropertyValueFactory<>("arrivalTime"));

        final TableColumn<TrainStop, String> column8 = new TableColumn<>("Departure");
        column8.setCellValueFactory(new PropertyValueFactory<>("departureTime"));

        final TableColumn<TrainStop, String> column9 = new TableColumn<>("Actual arrival");
        column9.setCellValueFactory(new PropertyValueFactory<>("actualArrivalTime"));

        final TableColumn<TrainStop, String> column10 = new TableColumn<>("Actual departure");
        column10.setCellValueFactory(new PropertyValueFactory<>("actualDepartureTime"));

        trainStopTableView.getColumns().addAll(column6, column7, column8, column9, column10);

        monitoringControl.setAlignment(Pos.CENTER);
        trainIdHBox.setPadding(new Insets(10));
        controlsHBox.setPadding(new Insets(10));
        trainIdHBox.getChildren().addAll(trainIdLabel, trainIdField);
        controlsHBox.getChildren().addAll(startButton, stopButton);
        infoHBox.getChildren().addAll(generalTextArea, trainStopTableView);

        this.addStartButtonListener(trainIdField, startButton, stopButton);
        this.addStopButtonListener(stopButton, startButton);
        monitoringControl.getChildren().addAll(trainIdHBox, controlsHBox, infoHBox);


        /*------------------- ROOT MANAGEMENT -----------------*/

        root.getChildren().addAll(solutionControl, monitoringControl);
        root.setColumnHalignment(HPos.LEFT);
        for (Node n : root.getChildren()) {
            FlowPane.setMargin(n, new Insets(20));
        }
        stage.setScene(new Scene(root, 1140, 640));
        stage.show();
        stage.setOnCloseRequest(event -> Runtime.getRuntime().halt(0));


        /*------------------- ACTIONS -----------------*/

    }

    private void manageStationResponse(final ComboBox<String> comboBox) {
        PauseTransition pause = new PauseTransition(Duration.seconds(1));
        comboBox.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            pause.setOnFinished(event -> {
                if (!newValue.isEmpty()) {
                    if (!newValue.matches("[A-Z-.'` ]*[|]S[0-9]*")) {
                        comboBox.getItems().clear();
                        Future<Buffer> future = trainInfo.getStation(newValue);
                        future.onComplete(handler -> {
                            Platform.runLater(() -> {
                                comboBox.getItems().addAll(future.result().toString().split("\\r?\\n"));
                                comboBox.show();
                            });
                        });
                    }
                } else {
                    comboBox.getItems().clear();
                }
            });
            pause.playFromStart();
        });
    }

    private void updateDepartureStation(final String station) {
        this.departureStation = station.substring(station.indexOf("|")+2);
        this.departureStation = this.departureStation.replaceFirst("0*", "");
    }

    private void updateArrivalStation(final String station) {
        this.arrivalStation = station.substring(station.indexOf("|")+2);
        this.arrivalStation = this.arrivalStation.replaceFirst("0*", "");
    }

    private void addSearchButtonListener(final Button button, final ComboBox<String> departureStation, final ComboBox<String> arrivalStation,
                                         final DatePicker date, final TextField time, final TableView<Solution> solutionTableView) {
        button.setOnAction(e -> {
            this.updateDepartureStation(departureStation.getValue());
            this.updateArrivalStation(arrivalStation.getValue());

            solutionTableView.getItems().clear();

            Future<Buffer> future = trainInfo.getTrainSolutions(this.departureStation, this.arrivalStation,
                    date.getValue(), LocalTime.of(Integer.parseInt(time.getText(0, 2)), Integer.parseInt(time.getText(3, 5)), 1));
            future.onSuccess(body -> {
                Platform.runLater(() -> {
                    TrainSolution trainSolution = body.toJsonObject().mapTo(TrainSolution.class);
                    if (trainSolution.getSolutions().isEmpty()) {
                        solutionTableView.setPlaceholder(new Label("No train found"));
                    } else {
                        trainSolution.getSolutions().forEach(s -> {
                            solutionTableView.getItems().add(s);
                        });
                    }
                });
            });
        });
    }

    private void addStartButtonListener(final TextField trainIdTextArea, final Button start, final Button stop) {
        start.setOnAction(e -> {
            if (!trainIdTextArea.getText().isEmpty()) {
                this.trainId = trainIdTextArea.getText();
                requestTimer = new Timer();
                requestTimer.schedule(new TrainTimerTask(Integer.parseInt(trainId), trainInfo, this),
                        0, 3*60000);
                start.setDisable(true);
                stop.setDisable(false);
            }
        });
    }

    private void addStopButtonListener(final Button stop, final Button start) {
        stop.setOnAction(e -> {
            requestTimer.cancel();
            generalTextArea.clear();
            trainStopTableView.getItems().clear();
            start.setDisable(false);
            stop.setDisable(true);
        });
    }

    public void updateTrainInfo(final Buffer body) {
        Platform.runLater(() -> {
            Train trainInfo = body.toJsonObject().mapTo(Train.class);
            generalTextArea.clear();
            generalTextArea.setText(trainInfo.toString());
            trainStopTableView.getItems().clear();
            trainInfo.getTrainStopList().forEach(t -> {
                trainStopTableView.getItems().add(t);
            });
        });
    }
}
