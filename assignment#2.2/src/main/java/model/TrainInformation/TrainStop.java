package model.TrainInformation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainStop {

    @JsonProperty("stazione")
    private String station;

    @JsonProperty("partenza_teorica")
    private Long departureTime;

    @JsonProperty("partenzaReale")
    private Long actualDepartureTime;

    @JsonProperty("arrivo_teorico")
    private Long arrivalTime;

    @JsonProperty("arrivoReale")
    private Long actualArrivalTime;

    public String getStation() {
        return station;
    }

    public String getArrivalTime() {
        if (arrivalTime != null) {
            return this.getHourAndMinute(arrivalTime);
        } else {
            return "//";
        }
    }

    public String getActualArrivalTime() {
        if (actualArrivalTime != null) {
            return this.getHourAndMinute(actualArrivalTime);
        } else {
            return "//";
        }
    }

    public String getDepartureTime() {
        if (departureTime != null) {
            return this.getHourAndMinute(departureTime);
        } else {
            return "//";
        }
    }

    public String getActualDepartureTime() {
        if (actualDepartureTime != null) {
            return this.getHourAndMinute(actualDepartureTime);
        } else {
            return "//";
        }
    }

    private String getHourAndMinute(final Long epochMillis) {
        final LocalTime time = Instant.ofEpochMilli(epochMillis)
                .atZone(ZoneId.systemDefault())
                .toLocalTime();

        final String minute = time.getMinute() <= 9 ? "0" + time.getMinute() : String.valueOf(time.getMinute());
        return time.getHour() + ":" + minute;
    }
}
