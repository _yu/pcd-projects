package model.TrainInformation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Train {
    @JsonProperty("categoria")
    private String trainCategory;

    @JsonProperty("numeroTreno")
    private String trainId;

    @JsonProperty("origine")
    private String departureStation;

    @JsonProperty("destinazione")
    private String arrivalStation;

    @JsonProperty("ritardo")
    private Long totalDelayInMinutes;

    @JsonProperty("oraUltimoRilevamento")
    private Long lastCheckTime;

    @JsonProperty("stazioneUltimoRilevamento")
    private String lastCheckStation;

    @JsonProperty("fermate")
    private List<TrainStop> trainStopList;

    public List<TrainStop> getTrainStopList() {
        return trainStopList;
    }

    public String toString() {
        return "The train " + this.trainCategory + " " + trainId +
                " from " + departureStation + " " + "to " + arrivalStation +
                getTrainStatus() + ".\n\n" +
                getMonitoring() + ".";
    }

    private String getMonitoring() {
        return !trainStopList.get(0).getActualDepartureTime().equals("//") ? "Last monitoring at " + lastCheckStation + " at " +
                getHourAndTime() : "No last monitoring";
    }

    private String getTrainStatus() {
        return trainStopList.get(0).getActualDepartureTime().equals("//") ?
                " has not left yet" : !trainStopList.get(trainStopList.size()-1).getActualArrivalTime().equals("//") ?
                " has arrived with " + getDelay() : " is traveling with " + getDelay();
    }

    private String getDelay() {
        return totalDelayInMinutes == 0 ? "no delay" : totalDelayInMinutes > 0 ?
                "a delay of " + totalDelayInMinutes + " minute"
                : "an advance of " + Math.abs(totalDelayInMinutes) + " minutes";
    }

    private String getHourAndTime() {
        final LocalTime time = Instant.ofEpochMilli(lastCheckTime)
                .atZone(ZoneId.systemDefault())
                .toLocalTime();

        final String minute = time.getMinute() <= 9 ? "0" + time.getMinute() : String.valueOf(time.getMinute());

        return time.getHour() + ":" + minute;
    }
}


