package model;

import infotreni.TrainInfo;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import view.ViewFX;

import java.util.TimerTask;

public class TrainTimerTask extends TimerTask {

    private int trainId;
    private TrainInfo trainInfo;
    private ViewFX view;

    public TrainTimerTask(final int trainId, final TrainInfo trainInfo, final ViewFX view) {
        super();
        this.trainId = trainId;
        this.trainInfo = trainInfo;
        this.view = view;
    }

    @Override
    public void run() {
        Future<Buffer> train = trainInfo.getRealTimeTrainInfo(this.trainId);
        train.onSuccess(body -> view.updateTrainInfo(body));
    }
}
