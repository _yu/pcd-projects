package model.TrainSolution;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicles {
    @JsonProperty("origine")
    private String departureStation;

    @JsonProperty("destinazione")
    private String arrivalStation;

    @JsonProperty("orarioPartenza")
    private String departureDateTime;

    @JsonProperty("orarioArrivo")
    private String arrivalDateTime;

    @JsonProperty("categoriaDescrizione")
    private String categoryDescription;

    @JsonProperty("numeroTreno")
    private String trainId;

    public Vehicles(){}

    public String getDepartureStation() {
        return departureStation;
    }

    public String getArrivalStation() {
        return arrivalStation;
    }

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public String getCategoryTrain() {
        return categoryDescription;
    }

    public String getTrainId() {
        return trainId;
    }
}
