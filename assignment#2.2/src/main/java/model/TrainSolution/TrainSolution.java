package model.TrainSolution;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainSolution {

    @JsonProperty("origine")
    private String departureStation;

    @JsonProperty("destinazione")
    private String arrivalStation;

    @JsonProperty("soluzioni")
    private List<Solution> solutions;

    public TrainSolution(){}

    public String getDepartureStation() {
        return departureStation;
    }

    public String getArrivalStation() {
        return arrivalStation;
    }

    public List<Solution> getSolutions() {
        return solutions;
    }

    public String toString() {
        return  "from: " + this.departureStation + "\n" +
                "to: " + this.arrivalStation + "\n";
    }
}
