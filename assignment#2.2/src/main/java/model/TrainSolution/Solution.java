package model.TrainSolution;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution {

    @JsonProperty("durata")
    private String duration;

    @JsonProperty("vehicles")
    private List<Vehicles> vehicles;

    private String departureTime;
    private String arrivalTime;
    private String trains;
    private String trainChanges;

    public Solution(){
    }

    public String getDuration() {
        return duration;
    }

    public List<Vehicles> getVehicles() {
        return vehicles;
    }

    public String getTrains() {
        this.trains = vehicles.stream().map(e -> {
            String newLine = vehicles.indexOf(e) != 0 ? "\n" : "";
            return new Pair<>(newLine+e.getCategoryTrain(), e.getTrainId());
        }).collect(Collectors.toList()).toString();
        return this.trains;
    }

    public String getTrainChanges(){
        final List<String> list = new ArrayList<>();
        if (vehicles.size() == 1) {
            list.add("No change");
        } else {
            IntStream.range(0, vehicles.size()-1).forEach(e -> {
                String newLine = e != 0 ? "\n" : "";
                list.add(newLine + vehicles.get(e).getArrivalStation());
            });
        }
        this.trainChanges = list.toString();
        return this.trainChanges;
    }

    public String getDepartureTime() {
        final String time = vehicles.get(0).getDepartureDateTime();
        this.departureTime = time.substring(time.indexOf("T")+1, time.length()-3);
        return this.departureTime;
    }

    public String getArrivalTime() {
        final String time = vehicles.get(vehicles.size()-1).getArrivalDateTime();
        this.arrivalTime = time.substring(time.indexOf("T")+1, time.length()-3);
        return this.arrivalTime;
    }

    public String toString(){
        return "duration: " + duration + "\n" +
                "departure time: " + this.getDepartureTime() + "\n" +
                "arrival time: " + this.getArrivalTime() + "\n" +
                "trains: " + this.getTrains() + "\n" +
                "change: " + this.getTrainChanges() + "\n";
    }

}
