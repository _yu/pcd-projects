import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class Test {
    public static void main(String[] args) {
        Flowable.range(1, 10)
                .flatMap(v -> {
                    log(v.toString());
                    return Flowable.just(v)
                          .subscribeOn(Schedulers.computation())
                          .map(w -> {
                              log(w.toString());
                              return w * w;
                          });
                })
                .blockingSubscribe(System.out::println);
    }

    static private void log(String msg) {
        System.out.println("[" + Thread.currentThread().getName() + " ] " + msg);
    }
}
