package controller;

public interface Controller {

    /**
     * Starts the application.
     */
    void launch(String directoryPath, String blacklistPath);

    /**
     * Starts computation.
     */
    void resume();

    /**
     * Stops computation
     */
    void pause();
}
