package controller;

import hu.akarnokd.rxjava3.operators.ObservableTransformers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.PublishSubject;
import model.PausableExecutor;
import model.WordCounterTask;
import view.ViewFX;
import java.io.File;
import java.util.*;

public class WordCounterService {

    private final PausableExecutor executor;
    private final Queue<File> filesQueue;
    private final List<String> blacklistedWords;
    private final ViewFX view;
    private boolean open;
    private PublishSubject<Boolean> source;

    public WordCounterService (final int poolSize,
                               final Collection<File> files,
                               final List<String> blacklistedWords,
                               final ViewFX view){
        this.executor = new PausableExecutor(poolSize);
        this.filesQueue = new ArrayDeque<>(files);
        this.blacklistedWords = blacklistedWords;
        this.view = view;
        this.open = true;
        this.source
             = PublishSubject.create();
    }

    public void compute() {

        Observable.fromStream(this.filesQueue.stream())
                .flatMap(file -> Observable.just(file)
                        .observeOn(Schedulers.from(this.executor))
                        .compose(ObservableTransformers.valve(this.source
            , this.open))
                        .map(WordCounterTask::getTextFromFile)
                        .compose(ObservableTransformers.valve(this.source
            , this.open))
                        .map(s -> WordCounterTask.generateOutput(s, this.blacklistedWords))
                )
                .reduce((o, acc) -> {
                    acc.updateWordCount(o.getWordCount());
                    o.getWordFrequency().forEach((word, num) -> acc.getWordFrequency()
                            .put(word, num + acc.getWordFrequency().getOrDefault(word,  0)));
                    if (this.open) view.updateView(acc);
                    return acc;
                })
                .subscribe(o -> {
                    this.executor.shutdown();
                });
    }


    public void pause() {
        this.open = false;
        this.source
            .onNext(this.open);
        this.executor.pause();
    }

    public void resume() {
        this.open = true;
        this.source
            .onNext(this.open);
        this.executor.resume();
    }

    public void addListener(ServiceListener listener){
        this.executor.addListener(listener);
    }
}
