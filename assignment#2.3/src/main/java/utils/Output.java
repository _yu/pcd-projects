package utils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class encapsulating the result of program's analysis,
 * for easier manipulation.
 */
public class Output{
    private int wordCount;
    private final Map<String, Integer> wordFrequency;

    public Output(int wordCount, Map<String, Integer> wordFrequency){
        this.wordCount = wordCount;
        this.wordFrequency = wordFrequency;
    }

    /**
     * @return Total number of words across all documents.
     */
    public int getWordCount(){
        return wordCount;
    }

    public void updateWordCount(int val){ wordCount += val; }

    /**
     * @return Mapping of each word and it's frequency across all documents.
     */
    public Map<String, Integer> getWordFrequency(){
        return wordFrequency;
    }

    /**
     * Get copy of this object to avoid unsafe access.
     * @return Copy of this output object.
     */
    public Output getCopy(){
        return new Output(wordCount, new LinkedHashMap<>(wordFrequency));
    }

}
