package p2p;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import app.Logger;
import app.PuzzleListener;
import app.Utils;
import puzzle.PuzzleBoard;

/**
 * This class implements the {@link RemotePeer} interface, to
 * expose the stub's remote methods, and the {@link Network} interface
 * to locally manage the contacts of the peer.
 */
public class Peer implements RemotePeer, LocalPeer {
	private Logger logger;
	private Set<RemotePeer> contacts = new HashSet<>();
	private PuzzleBoard puzzle;
	private RemoteLock lock;
	
	public Peer(Logger logger) {
		this.logger = logger;
		PuzzleListener listener = new PuzzleListener(this, logger);
		this.puzzle = new PuzzleBoard("localhost:"+logger.getPort(), Utils.n, Utils.m, Utils.imagePath, listener);
		this.puzzle.setVisible(true);
	}
	
	// remote interface methods

	@Override
	public Pair<Map<Integer, Integer>, Set<RemotePeer>> join(RemotePeer joiningPeer) throws RemoteException {
		getContacts().forEach(p -> { // propagate joining peer to contacts, for fully-connected p2p network
			try {
				p.addPeer(joiningPeer);
			} catch (RemoteException e) {
				logger.log("[RemoteException] could not propagate joining peer to " + Utils.getName(p));
			}
		});
		Set<RemotePeer> outContacts = getContacts();
		contacts.add(joiningPeer);
		joiningPeer.setRemoteLock(lock);
		logger.log("Peer " + Utils.getName(joiningPeer) + " just joined");
		logContacts();
		return new ImmutablePair<Map<Integer, Integer>, Set<RemotePeer>>(puzzle.getBoard(), outContacts);
	}

	@Override
	public void leave(RemotePeer leavingPeer) throws RemoteException {
		contacts.remove(leavingPeer);
		logger.log("Removed peer " + Utils.getName(leavingPeer) + " from contacts.");
	}
	
	@Override
	public void updateGame(RemotePeer peer, Map<Integer, Integer> newBoard) throws RemoteException {
		logger.log("Game update from " + Utils.getName(peer));
		setup(newBoard);
	}

	@Override
	public void addPeer(RemotePeer peer) throws RemoteException {
		contacts.add(peer);
		logContacts();
	}
	
	@Override
	public void displaySolution() throws RemoteException {
		puzzle.displaySolution();
	}

	@Override
	public RemoteLock getRemoteLock() throws RemoteException {
		return lock;
	}

	@Override
	public void setRemoteLock(RemoteLock lock) throws RemoteException {
		this.lock = lock;
	}
	
	@Override
	public Set<RemotePeer> getContacts() {
		return contacts.stream().collect(Collectors.toUnmodifiableSet());
	}

	// end remote methods; start local ones
	
	@Override
	public void addPeers(Set<RemotePeer> peers) {
		contacts.addAll(peers.stream().filter(p -> p != this).collect(Collectors.toSet()));
	}
	
	@Override
	public void removePeer(RemotePeer peer) {
		contacts.remove(peer);
	}
	
	@Override
	public void logContacts() {
		String s = getContacts().stream().map(p -> Utils.getName(p)).collect(Collectors.joining(", "));
		logger.log("Current contacts: " + s);
	}

	@Override
	public void setup(Map<Integer, Integer> newTiles) {
		puzzle.setTiles(newTiles);
		puzzle.paintPuzzle();
	}
	
	@Override
	public void showErrorMessage(String msg) {
		puzzle.showErrorMessage(msg);
	}

	@Override
	public void createRemoteLock() {
		try {
			this.lock = (RemoteLock) UnicastRemoteObject.exportObject(new Lock(), 0);
			getContacts().forEach(p -> {
				try {
					p.setRemoteLock(lock);
				} catch (RemoteException e) {
					logger.log("[ERROR] could not propagate new lock.");
				}
			});
		} catch (RemoteException e) {
			logger.log("[ERROR] could not create new lock.");
		}
		
	}

}
