package p2p;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Interface representing a remote peer in a p2p game network.
 * One can join a peer's network, leave it, update the peer's game.
 * The remote peer extends the {@link java.rmi.Remote} interface which
 * allows to remotely execute code on the peer.
 * 
 * @author Elena Rughi, Yuqi Sun
 */
public interface RemotePeer extends Remote {

	/**
	 * Adds joining peer to list of contacts, which means to join the joiningPeer's network.
	 * 
	 * @param joiningPeer The peer that wants to join.
	 * @return A pair that contains a map <tileId, tilePosition> that represents the puzzle board of the peer
	 * 			that is being joined, and a set containing the other peers in the network.
	 * @throws RemoteException
	 */
	Pair<Map<Integer, Integer>, Set<RemotePeer>> join(RemotePeer joiningPeer) throws RemoteException;
	
	/**
	 * Removes leaving peer from list of contacts.
	 * 
	 * @param leavingPeer The peer that wants to leave.
	 * @throws RemoteException
	 */
	void leave(RemotePeer leavingPeer) throws RemoteException;	

	/**
	 * Updates peer's game.
	 * 
	 * @param peer The peer who is providing the updated game.
	 * @param newBoard The updated board to display.
	 * @throws RemoteException
	 */
	void updateGame(RemotePeer peer, Map<Integer, Integer> newBoard) throws RemoteException;
	
	/**
	 * Add a remote peer to contacts.
	 * 
	 * @param peer The remote peer to add to network.
	 * @throws RemoteException
	 */
	void addPeer(RemotePeer peer) throws RemoteException;
	
	/**
	 * Display message when puzzle has been solved.
	 * 
	 * @throws RemoteException
	 */
	void displaySolution() throws RemoteException;
	
	/**
	 * Returns the reference to the network's remote lock.
	 * 
	 * @return The network's current remote lock.
	 * @throws RemoteException
	 */
	RemoteLock getRemoteLock() throws RemoteException;
	
	/**
	 * Updates peer's reference to the network's remote lock.
	 * Used to propagate the change in case past lock was lost.
	 * 
	 * @param lock The network's new remote lock.
	 * @throws RemoteException
	 */
	void setRemoteLock(RemoteLock lock) throws RemoteException;
	
	
	/**
	 * Get copy of list of connected peers.
	 * 
	 * @return The list of connected peers.
	 */
	Set<RemotePeer> getContacts() throws RemoteException;
	
}
