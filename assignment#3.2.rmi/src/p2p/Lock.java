package p2p;

import java.rmi.RemoteException;

public class Lock implements RemoteLock {
	private boolean isLocked = false;
	
	@Override
	public Boolean getLock() throws RemoteException {
		if (isLocked)
			return false;
		else {
			isLocked = true;
			return true;
		}
	}

	@Override
	public void releaseLock() throws RemoteException {
		isLocked = false;
	}

}
