package p2p;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteLock extends Remote {

	/**
	 * Ask for lock.
	 * 
	 * @return true if lock was reserved for the caller, false if it was already occupied.
	 * @throws RemoteException
	 */
	Boolean getLock() throws RemoteException;
	
	/**
	 * Release the lock.
	 * 
	 * @throws RemoteException
	 */
	void releaseLock() throws RemoteException;
	
}
