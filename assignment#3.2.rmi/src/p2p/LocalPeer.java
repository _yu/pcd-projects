package p2p;

import java.util.Map;
import java.util.Set;

/**
 * This interface contains methods to interact with the local instance of a peer.
 * 
 * @author Elena Rughi, Yuqi Sun
 */
public interface LocalPeer {
	
	/**
	 * Set the board to match the given one, represented by a mapping <tileId, tilePosition> 
	 * where tileId = tileOriginalPosition in {@link src.puzzle.Tile}
	 * 
	 * @param newTiles
	 */
	void setup(Map<Integer, Integer> newTiles);
	
	/**
	 * Add peers to contacts.
	 * 
	 * @param peers Peers to add to set of connected peers.
	 */
	void addPeers(Set<RemotePeer> peers);
	
	/**
	 * Remove peer from contacts.
	 * 
	 * @param peer The peer to remove from set of connected peers.
	 */
	void removePeer(RemotePeer peer);
	
	/**
	 * Print on stdout the formatted contacts of this peer.
	 */
	void logContacts();
	
	/**
	 * Open a dialog window to notify the user of a message.
	 * 
	 * @param msg The message shown to the user.
	 */
	void showErrorMessage(String msg);
	
	/**
	 * Makes a new lock for the network and propagates it.
	 */
	void createRemoteLock();
}
