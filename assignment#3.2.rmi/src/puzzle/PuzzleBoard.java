package puzzle;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

import app.PuzzleListener;

@SuppressWarnings("serial")
public class PuzzleBoard extends JFrame {
	
	final int rows, columns;
	private final List<Tile> tiles = new ArrayList<>();
	private final JPanel board;

	private final PuzzleListener listener;
	
	private final SelectionManager selectionManager = new SelectionManager();
	
    public PuzzleBoard(final String name, final int rows, final int columns, final String imagePath, final PuzzleListener listener) {
    	this.rows = rows;
		this.columns = columns;
		this.listener = listener;
    	
    	setTitle("Puzzle " + name);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        board = new JPanel();
        board.setBorder(BorderFactory.createLineBorder(Color.gray));
        board.setLayout(new GridLayout(rows, columns, 0, 0));
        getContentPane().add(board, BorderLayout.CENTER);

        createTiles(imagePath);
        paintPuzzle(board);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            	new Thread(() -> listener.notifyEnd()).start();
                super.windowClosing(e);
            }
        });
    }

    public void swapTiles(final int id1, final int id2) throws Exception {
        Tile t1 = getTile(id1);
        Tile t2 = getTile(id2);
        selectionManager.swap(t1, t2);

        paintPuzzle(board);
        checkSolution();
    }

    private Tile getTile(int id){
        Optional<Tile> ret = tiles.stream().filter(tile -> tile.getId() == id).findFirst();
        try {
            if (!ret.isPresent()) throw new Exception("Tile of id i" + id + " not in tiles");
        } catch (Exception e){
            System.err.println(e.getMessage());
        }
        return ret.get();
    }
    
    private void createTiles(final String imagePath) {
		final BufferedImage image;
        
        try {
            image = ImageIO.read(new File(imagePath));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Could not load image", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        final int imageWidth = image.getWidth(null);
        final int imageHeight = image.getHeight(null);

        int position = 0;
        
        final List<Integer> randomPositions = new ArrayList<>();
        IntStream.range(0, rows*columns).forEach(item -> { randomPositions.add(item); }); 
        Collections.shuffle(randomPositions);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
            	final Image imagePortion = createImage(new FilteredImageSource(image.getSource(),
                        new CropImageFilter(j * imageWidth / columns, 
                        					i * imageHeight / rows, 
                        					(imageWidth / columns), 
                        					imageHeight / rows)));
                Tile tile = new Tile(imagePortion, position, randomPositions.get(position));
                tiles.add(tile);
                position++;
            }
        }
	}

	public List<Tile> getCurrentTiles() {
        return this.tiles;
    }

    public void setTiles(final Map<Integer, Integer> newTiles) {
        this.tiles.forEach(tile -> {
            final int currentPos = newTiles.get(tile.getId());
            tile.setCurrentPosition(currentPos);
        });
    }

    public void paintPuzzle() {
        this.paintPuzzle(board);
    }

    private void paintPuzzle(final JPanel board) {
    	board.removeAll();
    	
    	Collections.sort(tiles);
    	
    	tiles.forEach(tile -> {
    		final TileButton btn = new TileButton(tile);
            board.add(btn);
            btn.setBorder(BorderFactory.createLineBorder(Color.gray));
            btn.addActionListener(actionListener -> {
            	selectionManager.selectTile(tile, (id1, id2) -> {
            		try {
						swapTiles(id1, id2);
					} catch (Exception e) {
						System.err.println("Could not swap tiles; " + e.getMessage());
					}
            		new Thread(() -> listener.notifySwap(getBoard())).start();
            	});
            });
    	});
    	
    	pack();
        //setLocationRelativeTo(null);
    }

    public void checkSolution() {
    	if(tiles.stream().allMatch(Tile::isInRightPlace)) {
    		displaySolution();
    		new Thread(() -> listener.notifySolved()).start();
    	}
    }
    
    public void displaySolution() {
		JOptionPane.showMessageDialog(this, "Puzzle Completed!", "", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void showErrorMessage(String msg) {
		JOptionPane.showMessageDialog(this, msg, "", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public Map<Integer, Integer> getBoard() {
		Map<Integer, Integer> out = new HashMap<>();
		tiles.forEach(t -> out.put(t.getId(), t.getCurrentPosition()));
		return out;
	}
}
