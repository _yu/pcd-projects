package app;

import java.rmi.RemoteException;
import java.util.Map;

import p2p.Peer;

/**
 * This listener links the GUI with the underlying {@link src.p2p.Peer}.
 * 
 * @author Elena Rughi, Yuqi Sun
 */
public class PuzzleListener {
	private Peer peer;
	private Logger logger;
	
	public PuzzleListener(Peer peer, Logger logger) { 
		this.peer = peer;
		this.logger = logger;
	}
	
	/**
	 * Notify that two tiles have been swapped.
	 * 
	 * @param newBoard The updated board after the swap.
	 */
	public void notifySwap(Map<Integer, Integer> newBoard) {
		try {
			if (peer.getRemoteLock().getLock()) {
				peer.getContacts().forEach(p -> {
					try {
						p.updateGame(peer, newBoard);
					} catch (RemoteException e) {
						logger.log("[ERROR] could not update " + Utils.getName(p));
						peer.removePeer(p);
						peer.showErrorMessage("Peer " + Utils.getName(p) + " not responding.\n Removing peer from contacts.");
					}
				});
				peer.getRemoteLock().releaseLock();
			} else {
				peer.showErrorMessage("Can't swap tiles, updating board right now. Try again in a bit.");
			}
		} catch (RemoteException e1) {
			logger.log("[ERROR] could not get remote lock reference from peer; creating new lock.");
			peer.createRemoteLock();
			notifySwap(newBoard);
		}
		
	}
	
	/**
	 * Notify that the window has been closed, thus initiating the 
	 * procedure of leaving the p2p network.
	 */
	public void notifyEnd() {
		peer.getContacts().forEach(p -> {
			try {
				p.leave(peer);
			} catch (RemoteException e) {
				logger.log("[RemoteException] Could not leave peer " + Utils.getName(p));
				peer.removePeer(p);
			}
		});
	}
	
	/**
	 * Notify that the puzzle has been solved.
	 */
	public void notifySolved() {
		peer.getContacts().forEach(p -> {
			try {
				p.displaySolution();
			} catch (RemoteException e) {
				logger.log("[RemoteException] Could not contact peer " + Utils.getName(p));
				peer.removePeer(p);
			}
		});
	}
	
}
