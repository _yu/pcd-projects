package app;

public class Logger {
	private int port;
	
	public Logger(int port) { this.port = port; }
	
	public void log(String msg) {
		System.out.println("[localhost:" + port + "] " + msg);
	}
	
	public int getPort() { return port; }
}
