package app;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import p2p.RemotePeer;

public class Utils {
	public static final int n = 3;
	public static final int m = 5;
	public static final String imagePath = "src/puzzle/bletchley-park-mansion.jpg";	
	
	public static final String stubnamePeer = "peer";
	public static final Pattern pattern = Pattern.compile("endpoint:(.*),objID");
		
	/**
	 * Extracts <host:port> from stub.
	 * 
	 * @param input Stub to extract address from.
	 * @return <host:port> of stub
	 */
	public static final String getName(RemotePeer input) {
		Pattern pattern = Pattern.compile("endpoint:(.*),objID");
		Matcher matcher = pattern.matcher(input.toString());
		matcher.find();
		return matcher.group(1);
	}
	
}
