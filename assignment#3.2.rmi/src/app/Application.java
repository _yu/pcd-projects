package app;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;

import p2p.*;

/**
 * Launches an instance of a puzzle that can participate in a p2p realized with Java RMI. 
 * Prompts the user to specify the port on which to expose the Peer stub
 * and the <host:port> of the peer to connect to. 
 * 
 * @author Elena Rughi, Yuqi Sun
 */
public class Application {
	private static Logger logger;
	
	public static void main(String[] args) {	
		System.out.println("Registry will be hosted on localhost");
		System.out.println("> Please input port to open registry on:");
		Scanner scanner = new Scanner(System.in);
		int port = scanner.nextInt();
		logger = new Logger(port);
		
		try {
			// create peer to participate in the p2p network
			Peer peer = new Peer(logger);	
	        RemotePeer peerStub = (RemotePeer) UnicastRemoteObject.exportObject(peer, 0);
	        
	        // create lock for the network
	        RemoteLock lock = (RemoteLock) UnicastRemoteObject.exportObject(new Lock(), 0);	
	        peer.setRemoteLock(lock);
            
	        // open Registry to expose peer's stub
            Registry registry =  LocateRegistry.createRegistry(port);
            logger.log("Registry open");
            registry.rebind(Utils.stubnamePeer, peerStub);
            logger.log("Peer registered on " + Utils.getName(peerStub));

            // make peer join a network
    		join(peer, lock);	
            
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
		scanner.close();
	}
	
	// connect to another peer's game
	static void join(Peer peer, RemoteLock lock) {
		// input which peer to connect to
		Scanner scanner = new Scanner(System.in);
		System.out.println("> Please input host to connect to; for localhost, simply press enter or write \"localhost\"");
		String peerHost = scanner.nextLine();
		if (peerHost == "localhost") peerHost = null;
		System.out.println("> Please input port to connect to:");
		int peerPort = scanner.nextInt();
		
		// try to connect to remote peer
		try {
			Registry extRegistry = LocateRegistry.getRegistry(peerHost, peerPort);
			RemotePeer extPeer = (RemotePeer) extRegistry.lookup(Utils.stubnamePeer); // retrieve remote peer
						
			Pair<Map<Integer, Integer>, Set<RemotePeer>> out = extPeer.join(peer);
			peer.setup(out.getLeft());		// setup board like other peer's
			peer.addPeers(Set.of(extPeer));	// add joined peer to contacts
			peer.addPeers(out.getRight());	// ... and peers connected to it
			logger.log("Joined network of peer " + Utils.getName(extPeer));
			peer.logContacts();
		} catch (Exception e) {
			logger.log("[ERROR] " + e.getMessage());
			e.printStackTrace();
			join(peer, lock);
		}
	}
	
	/* NOTES on rmiregistry
	 * 
	 * when creating a localhost registry application-side with the following code:
	 * 		LocateRegistry.createRegistry(port);
	 * do NOT run "rmiregistry [port]" command 
	 * 
	 * otherwise, run "rmiregistry [port]" command from bin/ folder and change code to:
	 * 		LocateRegistry.getRegistry(port);
	 * 
	 * default rmi port is 1099 
	 */
}
