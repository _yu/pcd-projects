package actors.chunks

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, StashBuffer}
import actors.chunks.Counter.CounterCommand

import java.util.Locale
import scala.collection.immutable.Map

object Counter {
  sealed trait CounterCommand
  final case class Terminate() extends CounterCommand
  final case class Count(chunk:String) extends CounterCommand
  final case object Pause extends CounterCommand
  final case object Resume extends CounterCommand
  final case class Resend(partialOutput:Map[String, Int], partialNumWords:Int) extends CounterCommand

  def apply(blacklistedWords:List[String], outputAggregator: ActorRef[OutputAggregator.OutputCommand]):Behavior[CounterCommand] =
    Behaviors.withStash(100) { buffer => {
        Behaviors.setup(context =>
        new Counter(context, blacklistedWords, outputAggregator, buffer).counter())
      }
    }
}

class Counter(ctx:ActorContext[Counter.CounterCommand],
              blacklistedWords:List[String],
              outputAggregator: ActorRef[OutputAggregator.OutputCommand],
              buffer: StashBuffer[CounterCommand]) {
  import Counter._

  ctx.setLoggerName(ctx.self.path.name)
  private var paused = false

  def counter(): Behavior[CounterCommand] =

    Behaviors.receiveMessage({
      case Count(chunk) =>
        val readyChunk = chunk.replaceAll("[^\\p{L} ]", "")
          .toLowerCase(Locale.ROOT)
          .split("\\s+")

        val frequency = readyChunk.filter(s => !blacklistedWords.contains(s))
          .foldLeft(Map.empty[String, Int]){
              (map, word) => map + (word -> (map.getOrElse(word, 0) + 1))
            }
        if (!paused) {
          outputAggregator ! OutputAggregator.ReceiveOutput(frequency, readyChunk.size)
        } else {
          buffer.stash(Resend(frequency, readyChunk.size))
        }
        Behaviors.same

      case Pause =>
        paused = true
        Behaviors.same

      case Resume =>
        paused = false
        buffer.unstashAll(Behaviors.same)
        Behaviors.same

      case Resend(partialOutput, partialNumWords) =>
        outputAggregator ! OutputAggregator.ReceiveOutput(partialOutput, partialNumWords)
        Behaviors.same

      case Terminate() =>
        Behaviors.stopped
    })
}
