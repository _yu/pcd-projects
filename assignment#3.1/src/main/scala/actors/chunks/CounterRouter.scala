package actors.chunks

import akka.actor.typed.{ActorRef, Behavior, Terminated}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, PoolRouter, Routers}
import utils.Utility.getFilterWords

object CounterRouter {
  sealed trait CounterRouterCommand
  final case class Work(chunk:String) extends CounterRouterCommand
  final case object Pause extends CounterRouterCommand
  final case object Resume extends CounterRouterCommand
  final case object Terminate extends CounterRouterCommand

  def apply(blacklistPath: String, outputAggregator: ActorRef[OutputAggregator.OutputCommand]):Behavior[CounterRouterCommand] =
    Behaviors.setup(context => new CounterRouter(context, blacklistPath, outputAggregator).counterRouter())
}

class CounterRouter(ctx:ActorContext[CounterRouter.CounterRouterCommand], blacklistPath:String, outputAggregator: ActorRef[OutputAggregator.OutputCommand]) {
  import CounterRouter._

  ctx.setLoggerName(ctx.self.path.name)

  val size: Int = Runtime.getRuntime.availableProcessors()
  val blacklistedWords:List[String] = if (blacklistPath.isEmpty) List.empty; else getFilterWords(blacklistPath)
  val pool: PoolRouter[Counter.CounterCommand] = Routers.pool(poolSize = size)(Counter.apply(blacklistedWords, outputAggregator))
  val router: ActorRef[Counter.CounterCommand] = ctx.spawn(pool, "worker-pool")
  ctx.watch(router)

  def counterRouter():Behavior[CounterRouterCommand] =
    Behaviors.receive[CounterRouterCommand]{ (_, message) => message match {
      case Work(chunk) =>
        router ! Counter.Count(chunk)
        Behaviors.same

      case Pause =>
        for (_ <- 0 until size) router ! Counter.Pause
        Behaviors.same

      case Resume =>
        for (_ <- 0 until size) router ! Counter.Resume
        Behaviors.same

      case Terminate =>
        for (_ <- 0 until size) router ! Counter.Terminate()
        Behaviors.same
    }}
    .receiveSignal{
      case (_, Terminated(_)) =>
        Behaviors.stopped
    }
}
