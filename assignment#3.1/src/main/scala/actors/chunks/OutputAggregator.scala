package actors.chunks

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import actors.controller.Controller

import scala.collection.immutable.Map
import scalaz._
import Scalaz._
import view.ViewFX

object OutputAggregator {
  sealed trait OutputCommand
  final case class ReceiveOutput(partialOutput:Map[String, Int], partialNumWords:Int) extends OutputCommand
  final case object Pause extends OutputCommand
  final case object Resume extends OutputCommand
  case object SendOutput extends OutputCommand

  def apply(controller: ActorRef[Controller.ControllerMessage], view:ViewFX): Behavior[OutputCommand] =
    Behaviors.setup { _ =>
      new OutputAggregator(controller, view).outputActor()
    }
}

class OutputAggregator(controller: ActorRef[Controller.ControllerMessage],
                       view:ViewFX) {
  import OutputAggregator._

  private var paused = false
  private var output:Map[String, Int] = Map.empty
  private var numWords:Int = 0

  private def outputActor():Behavior[OutputCommand] =
    Behaviors.receiveMessage{
      case ReceiveOutput(partialOutput, partialNumWords) =>
        output = output |+| partialOutput
        numWords += partialNumWords
        if (!paused) {
          view.updateView(output, numWords)
        }
        Behaviors.same

      case Pause =>
        paused = true
        Behaviors.same

      case Resume =>
        paused = false
        if (output.nonEmpty) view.updateView(output, numWords)
        Behaviors.same

      case SendOutput =>
        controller ! Controller.End()
        Behaviors.stopped
    }
}
