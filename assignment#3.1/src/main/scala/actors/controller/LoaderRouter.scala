package actors.controller

import akka.actor.typed.{ActorRef, Behavior, Terminated}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, PoolRouter, Routers}

import java.io.File

object LoaderRouter {
  sealed trait LoaderRouterCommand
  final case class Work(file: File) extends LoaderRouterCommand
  final case object TerminateLoaders extends LoaderRouterCommand
  final case object ChildNotification extends LoaderRouterCommand
  final case object Pause extends LoaderRouterCommand
  final case object Resume extends LoaderRouterCommand

  def apply():Behavior[LoaderRouterCommand] =
    Behaviors.setup(context => new CounterRouter(context).counterRouter())
}

class CounterRouter(ctx:ActorContext[LoaderRouter.LoaderRouterCommand]) {
  import LoaderRouter._

  ctx.setLoggerName(ctx.self.path.name)

  var size: Int = Runtime.getRuntime.availableProcessors()
  val pool: PoolRouter[Loader.LoaderMessage] = Routers.pool(poolSize = size)(Loader.apply(ctx.self))
  val router: ActorRef[Loader.LoaderMessage] = ctx.spawn(pool, "loader-worker-pool")
  ctx.watch(router)

  def counterRouter():Behavior[LoaderRouterCommand] =
    Behaviors.receive[LoaderRouterCommand]{ (_, message) => message match {
      case Work(file) =>
        router ! Loader.Work(file)
        Behaviors.same

      case Pause =>
        for (_ <- 0 until size) router ! Loader.Pause
        Behaviors.same

      case Resume =>
        for (_ <- 0 until size) router ! Loader.Resume
        Behaviors.same

      case ChildNotification =>
        size -= 1
        Behaviors.same

      case TerminateLoaders =>
        for (_ <- 0 until size) router ! Loader.Terminate()
        Behaviors.same
      }
    }
    .receiveSignal{
      case (_, Terminated(_)) =>
        Behaviors.stopped
    }
}
