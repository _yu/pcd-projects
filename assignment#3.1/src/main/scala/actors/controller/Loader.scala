package actors.controller
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, StashBuffer}
import actors.Supervisor
import actors.chunks.CounterRouter
import actors.controller.Loader.LoaderMessage
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper

import java.io.IOException
import java.io.File

object Loader {

  sealed trait LoaderMessage
  final case class Terminate() extends LoaderMessage
  final case class Work(file:File) extends LoaderMessage
  final case class LoadFile(file: File) extends LoaderMessage
  final case class SendDocument(listing: Receptionist.Listing) extends LoaderMessage
  final case object Pause extends LoaderMessage
  final case object Resume extends LoaderMessage

  def apply(router:ActorRef[LoaderRouter.LoaderRouterCommand]): Behavior[LoaderMessage] =
    Behaviors.withStash(100) { buffer =>
      Behaviors.setup { context =>
        new Loader(context, buffer, router).loader(null, 0, working = false)
      }
    }
}

class Loader(ctx: ActorContext[LoaderMessage],
             buffer: StashBuffer[LoaderMessage],
             router:ActorRef[LoaderRouter.LoaderRouterCommand]) {

  import Loader._

  ctx.setLoggerName(ctx.self.path.name)
  private var paused = false
  private val stripper = new PDFTextStripper()
  private val listingResponseAdapter: ActorRef[Receptionist.Listing] = ctx.messageAdapter[Receptionist.Listing](SendDocument)

  private def loader(doc:PDDocument, numPages:Int, working:Boolean):Behavior[LoaderMessage] =

    Behaviors.receiveMessage {
      case LoadFile(file) =>
        val document = PDDocument.load(file)
        val ap = document.getCurrentAccessPermission
        if (!ap.canExtractContent) {
          throw new IOException("You do not have permission to extract text")
        }
        ctx.system.receptionist ! Receptionist.Find(Supervisor.CounterRouterServiceKey, listingResponseAdapter)
        loader(document, document.getNumberOfPages, working)

      case SendDocument(Supervisor.CounterRouterServiceKey.Listing(listings)) =>
        if (!paused) {
          var tmpWorking = true
          if (numPages > 0) {
            stripper.setStartPage(numPages)
            stripper.setEndPage(numPages)
            val chunk = stripper.getText(doc)
            listings.head ! CounterRouter.Work(chunk)
            ctx.system.receptionist ! Receptionist.Find(Supervisor.CounterRouterServiceKey, listingResponseAdapter)
            loader(doc, numPages-1, tmpWorking)
          } else {
            doc.close()
            tmpWorking = false
            buffer.unstash(loader(null, 0, tmpWorking), 1, s => s)
          }
        } else {
          Behaviors.same
        }

      case Pause =>
        paused = true
        Behaviors.same

      case Resume =>
        paused = false
        if (working && buffer.nonEmpty) {
          ctx.system.receptionist ! Receptionist.Find(Supervisor.CounterRouterServiceKey, listingResponseAdapter)
        }
        Behaviors.same

      case Work(file) =>
        if (!working && !paused) {
          ctx.self ! LoadFile(file)
          loader(null, 0, working = true)
        } else {
          buffer.stash(Work(file))
          Behaviors.same
        }

      case Terminate() =>
        if (buffer.isEmpty && !working) {
          router ! LoaderRouter.ChildNotification
          Behaviors.stopped
        } else {
          buffer.stash(Terminate())
          Behaviors.same
        }
    }
}
