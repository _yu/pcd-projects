package actors.controller
import akka.actor.typed.receptionist.Receptionist
import utils.Utility._
import org.apache.commons.lang3.time.StopWatch

import scala.collection.immutable.Queue
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import actors.Supervisor
import view.ViewFX

import java.io.File

object Controller {

  sealed trait ControllerMessage
  case object SetUp extends ControllerMessage
  final case class SendFile(listing: Receptionist.Listing) extends ControllerMessage
  final case object Pause extends ControllerMessage
  final case object Resume extends ControllerMessage
  case class End() extends ControllerMessage

  def apply(directoryPath: String, view:ViewFX): Behavior[ControllerMessage] =
    Behaviors.setup { context =>
      new Controller(context, directoryPath, view).controller(Queue.empty, 0)
    }

}

class Controller(ctx: ActorContext[Controller.ControllerMessage], directoryPath: String, view: ViewFX) {
  import Controller._

  private var paused = false
  private val stopwatch: StopWatch = new StopWatch()
  private val listingResponseAdapter: ActorRef[Receptionist.Listing] = ctx.messageAdapter[Receptionist.Listing](SendFile)

  private def controller(fileNamesQueue: Queue[File], numLoaderChildren:Int): Behavior[ControllerMessage] =
    Behaviors.receiveMessage {
      case SetUp =>
        val fileNames = Queue.from(getFiles(directoryPath))
        stopwatch.reset()
        stopwatch.start()
        ctx.system.receptionist ! Receptionist.Find(Supervisor.LoaderRouterServiceKey, listingResponseAdapter)
        controller(fileNames, fileNames.length)

      case SendFile(Supervisor.LoaderRouterServiceKey.Listing(listings)) =>
        if (!paused) {
          if (fileNamesQueue.nonEmpty) {
            val option = fileNamesQueue.dequeueOption
            listings.head ! LoaderRouter.Work(option.get._1)
            ctx.system.receptionist ! Receptionist.Find(Supervisor.LoaderRouterServiceKey, listingResponseAdapter)
            controller(option.get._2, numLoaderChildren)
          } else {
            listings.head ! LoaderRouter.TerminateLoaders
            Behaviors.same
          }
        } else {
          Behaviors.same
        }

      case Pause =>
        paused = true
        stopwatch.suspend()
        Behaviors.same

      case Resume =>
        paused = false
        stopwatch.resume()
        Behaviors.same

      case End() =>
        stopwatch.stop()
        view.updateTime(stopwatch.getTime)
        Behaviors.stopped
    }
}
