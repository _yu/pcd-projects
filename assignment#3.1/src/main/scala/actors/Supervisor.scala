package actors

import akka.actor.typed.receptionist.{Receptionist, ServiceKey}
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import actors.chunks.{CounterRouter, OutputAggregator}
import actors.controller.{Controller, LoaderRouter}
import view.ViewFX

object Supervisor {

  sealed trait SupervisorCommand
  final case object Start extends SupervisorCommand
  final case object Stop extends SupervisorCommand
  final case object Pause extends SupervisorCommand
  final case object Resume extends SupervisorCommand
  final case class LoaderNotification(loaderRouter:ActorRef[LoaderRouter.LoaderRouterCommand]) extends SupervisorCommand
  final case class CounterNotification(counterRouter: ActorRef[CounterRouter.CounterRouterCommand]) extends SupervisorCommand

  val LoaderRouterServiceKey: ServiceKey[LoaderRouter.LoaderRouterCommand] = ServiceKey[LoaderRouter.LoaderRouterCommand]("LoaderRouterService")
  val CounterRouterServiceKey: ServiceKey[CounterRouter.CounterRouterCommand] = ServiceKey[CounterRouter.CounterRouterCommand]("CounterRouterService")

  def apply(directoryPath: String, blacklistPath: String, view:ViewFX):Behavior[SupervisorCommand] =
    Behaviors.setup {
      context => new Supervisor(context, directoryPath, blacklistPath, view).supervisor()
    }
}

class Supervisor(ctx:ActorContext[Supervisor.SupervisorCommand], directoryPath: String,
                 blacklistPath: String, view:ViewFX) {
  import Supervisor._

  private var outputAggregator:Option[ActorRef[OutputAggregator.OutputCommand]] = None
  private var loaderRouter:Option[ActorRef[LoaderRouter.LoaderRouterCommand]] = None
  private var counterRouter:Option[ActorRef[CounterRouter.CounterRouterCommand]] = None
  private val controller = ctx.spawn(Controller.apply(directoryPath, view), "Controller")

  def supervisor():Behavior[SupervisorCommand] =
    Behaviors.receiveMessage({
      case Start =>
        outputAggregator = Option.apply(ctx.spawn(OutputAggregator.apply(controller, view), "OutputAggregator"))
        loaderRouter = Option.apply(ctx.spawn(LoaderRouter.apply(), "LoaderRouter"))
        counterRouter = Option.apply(ctx.spawn(CounterRouter.apply(blacklistPath, outputAggregator.get), "CounterRouter"))

        ctx.system.receptionist ! Receptionist.Register(LoaderRouterServiceKey, loaderRouter.get)
        ctx.watchWith(loaderRouter.get,LoaderNotification(loaderRouter.get))

        ctx.system.receptionist ! Receptionist.Register(CounterRouterServiceKey, counterRouter.get)
        ctx.watchWith(counterRouter.get, CounterNotification(counterRouter.get))
        controller ! Controller.SetUp
        Behaviors.same

      case Pause =>
        controller ! Controller.Pause
        loaderRouter.get ! LoaderRouter.Pause
        counterRouter.get ! CounterRouter.Pause
        outputAggregator.get ! OutputAggregator.Pause
        Behaviors.same

      case Resume =>
        controller ! Controller.Resume
        loaderRouter.get ! LoaderRouter.Resume
        counterRouter.get ! CounterRouter.Resume
        outputAggregator.get ! OutputAggregator.Resume
        Behaviors.same

      case LoaderNotification(_) =>
        counterRouter.get ! CounterRouter.Terminate
        Behaviors.same

      case CounterNotification(_) =>
        outputAggregator.get ! OutputAggregator.SendOutput
        Behaviors.same

      case Stop =>
        Behaviors.stopped
    })
}
