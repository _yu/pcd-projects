import scalafx.application.JFXApp3
import scalafx.application.JFXApp3.PrimaryStage
import view.ViewFX

object Application extends JFXApp3 {

  override def start(): Unit = {
    val stage = new PrimaryStage
    new ViewFX(stage).apply()
  }
}
