package utils
import java.io.{File, FileNotFoundException}
import scala.io.{BufferedSource, Source}
object Utility {

  def getFilterWords(file:String): List[String] = {
    var bufferedSource : BufferedSource = null
    try {
      bufferedSource = Source.fromFile(file)
    } catch {
      case e: FileNotFoundException => println(">>> Invalid blacklist file."); System.exit(1)
    }
    val words = bufferedSource.getLines.toList
    bufferedSource.close()
    words
  }

  def getFiles(directory:String): List[File] = {
    val dir = new File(directory)
    if (dir.listFiles() == null) {
      println(">>> Invalid directory")
      System.exit(1)
    }
    dir.listFiles.filter(_.isFile).toList.filter(file => {
        file.getName.endsWith(".pdf")})
  }

}
