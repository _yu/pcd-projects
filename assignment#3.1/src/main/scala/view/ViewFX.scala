package view
import akka.actor.typed.ActorSystem
import actors.Supervisor
import actors.Supervisor.SupervisorCommand
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.control.{Button, Label, ScrollPane, TextArea, TextField, TextFormatter}
import scalafx.stage.DirectoryChooser
import scalafx.stage.FileChooser
import scalafx.scene.layout.{BorderPane, FlowPane, HBox}
import scalafx.scene.Scene
import scalafx.Includes._
import scalafx.application.Platform


class ViewFX(stage:PrimaryStage) {
  private val textArea:TextArea = new TextArea()
  private val inputNumWords:TextField = new TextField()
  private val start:Button = new Button("Start")
  private val stop:Button = new Button("Pause")
  private val resume:Button = new Button("Resume")
  private val openFolder:Button = new Button("Choose folder")
  private val openFilterFile:Button = new Button("Choose filter")
  private val fileChooser = new FileChooser
  private val directoryChooser = new DirectoryChooser
  private var system:Option[ActorSystem[SupervisorCommand]] = None


  stage.title = "Word Counter"

  def apply():Unit = {

    val root = new FlowPane

    /*--------- PATH CONTROLS ---------*/

    val parametersPane = new FlowPane(15, 15)

    val folderBox = new HBox(5)
    openFolder.prefWidth = 100
    val folderPath:TextField = new TextField()
    folderPath.editable = false
    folderPath.prefColumnCount = 30
    folderPath.promptText = "No folder chosen"

    folderBox.children.addAll(openFolder, folderPath)

    /*---------------------------------------*/

    val filterBox:HBox = new HBox(5)

    openFilterFile.prefWidth = 100
    val filterPath:TextField = new TextField()
    filterPath.editable = false
    filterPath.prefColumnCount = 30
    filterPath.promptText = "No filter chosen"

    filterBox.children.addAll(openFilterFile, filterPath)

    /*---------------------------------------*/

    val numWordsLabel:Label = new Label("Choose number of words (default: 10)")
    inputNumWords.textFormatter = new TextFormatter[TextFormatter.Change](getFilter)
    inputNumWords.text = "10"

    parametersPane.children.addAll(folderBox, filterBox, numWordsLabel, inputNumWords)

    /*--------- APP CONTROLS ---------*/

    val appControlPane = new FlowPane(15, 15)
    appControlPane.alignment = scalafx.geometry.Pos.Center

    start.prefWidth = 80
    stop.prefWidth = 80
    resume.prefWidth = 80
    start.disable = true
    stop.disable = true
    resume.disable = true

    appControlPane.children.addAll(start, stop, resume)

    val panel = new BorderPane
    panel.left = parametersPane
    panel.right = appControlPane

    /*--------- DISPLAY ---------*/

    val display = new FlowPane(15, 15)
    textArea.prefColumnCount = 69
    textArea.prefRowCount = 18
    textArea.editable = false
    textArea.wrapText = true

    val scrollPane:ScrollPane = new ScrollPane
    scrollPane.content = textArea
    scrollPane.fitToHeight = true
    scrollPane.fitToWidth = true

    display.children.addAll(scrollPane)

    /*------------------- ROOT MANAGEMENT -----------------*/

    root.children.addAll(panel, display)
    root.children.foreach(n => {
      FlowPane.setMargin(n, Insets.apply(15))
    })

    stage.scene = new Scene(root, 840, 490)
    stage.show

    /* -------------------- ACTIONS ----------------------*/
    openFolder.onAction = _ => {
      val selectedDirectory = directoryChooser.showDialog(stage)
      if (selectedDirectory != null) {
        folderPath.text = selectedDirectory.getAbsolutePath
        start.disable = false
      }
    }

    openFilterFile.onAction = _ => {
      val selectedFile = fileChooser.showOpenDialog(stage)
      if (selectedFile != null) filterPath.setText(selectedFile.getAbsolutePath)
    }

    start.onAction = _ => {
      textArea.clear()
      if (filterPath.text.isEmpty.get())
        textArea.appendText("The program won't filter any words because no filter file was chosen.\n")
      start.disable = true
      stop.disable = false
      resume.disable = true
      inputNumWords.disable = true
      openFolder.disable = true
      openFilterFile.disable = true
      textArea.appendText("Processing...\n")
      val finalPath = if (filterPath.getText.isEmpty) "" else filterPath.getText
      system = Option.apply(ActorSystem[SupervisorCommand]
        (Supervisor(folderPath.text.get(), finalPath, this), "supervisor"))
      system.get ! Supervisor.Start
    }


    stop.onAction = _ => {
      system.get ! Supervisor.Pause
      stop.disable = true
      resume.disable = false
    }

    resume.onAction = _ => {
      system.get ! Supervisor.Resume
      stop.setDisable(false)
      resume.setDisable(true)
    }
  }

  def updateView(output: Map[String, Int], numWords:Int): Unit = {
    Platform.runLater(() => {
      this.textArea.clear()
      this.textArea.appendText("NUMBER OF PROCESSED WORDS: " + numWords + "\n")
      this.textArea.appendText("------------------------------------------------------\n")
      this.textArea.appendText("TOP " + this.inputNumWords.text.get() + " WORDS:\n")
      output.toSeq
        .sortWith(_._2 > _._2)
        .take(this.inputNumWords.text.get().toInt)
        .foreach(e => this.textArea.appendText("{" + e.getKey + ": " + e.getValue + "}\n"))
    })
  }

  def updateTime(time: Long): Unit = {
    start.disable = false
    stop.disable = true
    resume.disable = true
    inputNumWords.disable = false
    openFolder.disable = false
    openFilterFile.disable = false
    Platform.runLater(() => {
      this.textArea.appendText("------------------------------------------------------\n")
      this.textArea.appendText("ELAPSED TIME: " + time + "ms\n")
    })
  }

  private def getFilter = (t: TextFormatter.Change) => {
    def foo(t: TextFormatter.Change) = {
      if (t.text.matches("[^0-9]")) t.text = t.controlText.substring(t.rangeStart, t.rangeEnd)
      if (t.controlText.contains(".")) if (t.text.matches("[^0-9]")) t.text = ""
      else if (t.text.matches("[^0-9.]")) t.text = ""
      t
    }

    foo(t)
  }

}