package model;

import utils.Output;

import java.util.*;

//TODO change name of monitor
public class OutputMonitor {

    private final Map<String, Integer> wordFrequency = new LinkedHashMap<>();
    private int wordCounter = 0;

    public synchronized void update(Output output){
        wordCounter += output.getWordCount();
        output.getWordFrequency().forEach((word, num) -> {
            wordFrequency.put(word, num + wordFrequency.getOrDefault(word, 0));
        });
    }

    /**
     * @return a copy of the current output.
     */
    public synchronized Output getOutput() {
        return new Output(wordCounter, new LinkedHashMap<>(wordFrequency));
    }
}
