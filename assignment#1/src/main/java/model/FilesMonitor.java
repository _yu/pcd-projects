package model;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;

/**
 * Monitor containing the queue of .pdf files to be read.
 */
public class FilesMonitor {
    private final Queue<File> files;

    public FilesMonitor(Collection<File> files){
        this.files = new ArrayDeque<>(files);
    }

    /**
     * Get next file in queue of files to read, removing it
     * from the queue.
     * @return File to be read
     */
    public synchronized File getFile(){
        if(files.isEmpty()) {
            return null;
        } else {
            return files.poll();
        }
    }
}
