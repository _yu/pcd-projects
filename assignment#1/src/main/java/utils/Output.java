package utils;

import java.util.Map;

/**
 * Class encapsulating the result of program's analysis,
 * for easier manipulation.
 */
public class Output{
    private final int wordCount;
    private final Map<String, Integer> wordFrequency;

    public Output(int wordCount, Map<String, Integer> wordFrequency){
        this.wordCount = wordCount;
        this.wordFrequency = wordFrequency;
    }

    /**
     * @return Total number of words across all documents.
     */
    public int getWordCount(){
        return wordCount;
    }

    /**
     * @return Mapping of each word and it's frequency across all documents.
     */
    public Map<String, Integer> getWordFrequency(){
        return wordFrequency;
    }

}
