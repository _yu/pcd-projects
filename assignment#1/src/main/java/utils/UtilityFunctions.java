package utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.SizeFileComparator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

public class UtilityFunctions {

    /**
     * Get words to blacklist from output.
     * @param file Textual file containing one word per line
     * @return List of blacklisted words as Strings
     */
    public static List<String> getFilterWords(final File file) {
        if (file != null) {
            try {
                String data = FileUtils.readFileToString(file, "UTF-8");
                return Arrays.stream(data.replaceAll("\\p{Punct}", "")
                        .toLowerCase()
                        .split("\\s+"))
                        .collect(Collectors.toList());
            } catch (IOException e) {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Get paths of .pdf files inside specified directory,
     * in descending order by file size.
     * @param directory directory containing files to be read
     * @return List of File objects
     */
    public static List<File> getOrderedFiles(File directory){
        return Arrays.stream(Objects.requireNonNull(directory.listFiles((dir, name) -> name.endsWith(".pdf"))))
              .sorted(SizeFileComparator.SIZE_REVERSE)
              .collect(Collectors.toList());
    }

    // for testing
    public static void printOutput(Output output, int n){
        Map<String, Integer> wordFrequency = output.getWordFrequency();
        Map<String, Integer> resMap = new LinkedHashMap<>();
        wordFrequency.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(n)
                .forEachOrdered(e -> resMap.put(e.getKey(), e.getValue()));

        System.out.println("------------------------------------\n");
        System.out.println("resMap empty " + resMap.isEmpty());
        resMap.forEach((word, num) -> {
            System.out.println("{" + word + ": " + num + "}");
        });
        System.out.println("------------------------------------");
        System.out.println("Word count: " + output.getWordCount());
    }
}
