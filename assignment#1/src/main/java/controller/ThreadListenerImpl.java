package controller;

import model.OutputMonitor;
import org.apache.commons.lang3.time.StopWatch;
import view.ViewFX;

import java.util.List;
import java.lang.Thread.State;

public class ThreadListenerImpl implements ThreadListener {

    private final List<ReaderAgent> threads;
    private final StopWatch watch;
    private final OutputMonitor outputMonitor;
    private final ViewFX view;

    public ThreadListenerImpl(final List<ReaderAgent> threads, final StopWatch watch, final OutputMonitor outputMonitor, final ViewFX view){
        this.threads = threads;
        this.watch = watch;
        this.outputMonitor = outputMonitor;
        this.view = view;
    }

    public void notifyView() {
        view.updateView(outputMonitor.getOutput());
    }

    @Override
    public void notifyTermination(Thread caller) {
        int nDead = 0;
        for (Thread t : threads){
            if (t.getState() == State.TERMINATED) nDead++;
        }

        // if caller thread is the last active reader, stop the time
        if(threads.size() - nDead == 1){
            watch.stop();
            view.updateTime(watch.getTime());
        }
    }
}
