package controller;

import model.FilesMonitor;
import model.OutputMonitor;
import org.apache.commons.lang3.time.StopWatch;
import utils.UtilityFunctions;
import view.ViewFX;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class ControllerImpl implements Controller{

    private final static int NUM_THREAD = Runtime.getRuntime().availableProcessors() + 2;
    private final StopWatch watch;
    private final List<ReaderAgent> threads;
    private final ViewFX view;

    public ControllerImpl(final ViewFX view) {
        this.view = view;
        this.watch = new StopWatch();
        this.threads = new LinkedList<>();
    }

    @Override
    public void launch(final String directoryPath, final String blacklistPath) {
        final List<String> blacklistedWords = UtilityFunctions.getFilterWords(blacklistPath == null ? null : new File(blacklistPath));
        final FilesMonitor filesMonitor = new FilesMonitor(UtilityFunctions.getOrderedFiles(new File(directoryPath)));
        final OutputMonitor outputMonitor = new OutputMonitor();
        final ThreadListener listener = new ThreadListenerImpl(threads, watch, outputMonitor, this.view);

        threads.clear();
        for (int i : IntStream.range(0, NUM_THREAD).toArray()){
            threads.add(new ReaderAgent("Reader" + i,
                    blacklistedWords, filesMonitor, outputMonitor, listener,false));
        }
        watch.reset();
        watch.start();
        threads.forEach(Thread::start);
    }

    @Override
    public synchronized void resume() {
        threads.forEach(ReaderAgent::resumeThread);
    }

    @Override
    public synchronized void pause() {
        threads.forEach(ReaderAgent::pause);
    }
}
