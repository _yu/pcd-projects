package controller;

public interface ThreadListener {

    /**
     * Called by ReaderAgents just before terminating their execution.
     * @param caller Reference to notifying thread
     */
    void notifyTermination(Thread caller);

    /**
     * Called by ReaderAgents each time they finish reading a pdf file
     * in order to update the user output.
     */
    void notifyView();
}
