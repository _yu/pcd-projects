package controller;

import model.FilesMonitor;
import model.OutputMonitor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;
import utils.Output;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ReaderAgent extends Thread{

    private final String name;
    private final List<String> blacklistedWords;
    private final OutputMonitor outputMonitor;
    private final FilesMonitor filesMonitor;
    private final ThreadListener listener;
    private final boolean debug;
    private boolean stop;
    private final Object pauseLock = this;

    public ReaderAgent(String name, List<String> blacklistedWords,
                       FilesMonitor filesMonitor, OutputMonitor outputMonitor,
                       ThreadListener listener, boolean debug) {
        super(name);
        this.name = name;
        this.blacklistedWords = blacklistedWords;
        this.outputMonitor = outputMonitor;
        this.filesMonitor = filesMonitor;
        this.listener = listener;
        this.debug = debug;
        stop = false;
    }

    public void run(){
        if(debug) log("started");

        File file = filesMonitor.getFile();
        while (file != null){
            try {
                PDDocument document = PDDocument.load(file);
                AccessPermission ap = document.getCurrentAccessPermission();
                if (!ap.canExtractContent())
                    throw new IOException("You do not have permission to extract text");
                PDFTextStripper stripper = new PDFTextStripper();
                String text = stripper.getText(document);
                document.close();
                outputMonitor.update(generateOutput(text));
                this.pauseThread();
                listener.notifyView();
                if(debug) log("done reading " + file.getName());
                file = filesMonitor.getFile();
            } catch (IOException e) { e.printStackTrace(); }
        }

        if(debug) log("done");
        listener.notifyTermination(currentThread());
    }

    public void pause() {
        this.stop = true;
    }

    public void resumeThread() {
        this.stop = false;
        synchronized (pauseLock) {
            pauseLock.notify();
        }
    }

    private void pauseThread() {
        synchronized (pauseLock) {
            if (this.stop) {
                try {
                    pauseLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Output generateOutput(String text){
        this.pauseThread();
        Map<String, Integer> wordFrequency = new LinkedHashMap<>();

        final String[] words = text.replaceAll("[^\\p{L} ]", "").toLowerCase(Locale.ROOT).split("\\s+");
        Arrays.stream(words)
              .filter(w -> !blacklistedWords.contains(w))
              .forEach(w -> {
                  if (wordFrequency.putIfAbsent(w, 1) != null)
                      wordFrequency.put(w, wordFrequency.get(w) + 1);
              });

        return new Output(words.length, wordFrequency);
    }

    private void log(String msg){
        synchronized(System.out){
            System.out.println("[" + this.name + "] " + msg);
        }
    }
}
