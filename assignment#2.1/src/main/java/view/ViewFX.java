package view;

import controller.Controller;
import controller.ControllerImpl;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utils.Output;

import java.io.File;
import java.util.Comparator;
import java.util.Map;
import java.util.function.UnaryOperator;

public class ViewFX {

    private final Controller controller;
    private final TextArea textArea = new TextArea();
    final TextField inputNumWords = new TextField();
    final Button start = new Button("Start");
    final Button stop = new Button("Pause");
    final Button resume = new Button("Resume");
    final Button openFolder = new Button("Choose folder");
    final Button openFilterFile = new Button("Choose filter");
    private final FileChooser fileChooser = new FileChooser();
    private final DirectoryChooser directoryChooser = new DirectoryChooser();

    public ViewFX(final Stage stage) {
        this.controller = new ControllerImpl(this);
        stage.setTitle("Word Counter");
        final FlowPane root = new FlowPane();

        /*--------- PATH CONTROLS ---------*/
        final FlowPane parametersPane = new FlowPane(15, 15);

        final HBox folderBox = new HBox(5);
        openFolder.setPrefWidth(100);
        final TextField folderPath = new TextField();
        folderPath.setEditable(false);
        folderPath.setPrefColumnCount(30);
        folderPath.setPromptText("No folder chosen");

        folderBox.getChildren().addAll(openFolder, folderPath);

        /*---------------------------------------*/

        final HBox filterBox = new HBox(5);

        openFilterFile.setPrefWidth(100);
        final TextField filterPath = new TextField();
        filterPath.setEditable(false);
        filterPath.setPrefColumnCount(30);
        filterPath.setPromptText("No filter chosen");

        filterBox.getChildren().addAll(openFilterFile, filterPath);

        /*---------------------------------------*/

        final Label numWordsLabel = new Label("Choose number of words (default: 10)");
        inputNumWords.setTextFormatter(new TextFormatter<>(getFilter()));
        inputNumWords.setText("10");

        parametersPane.getChildren().addAll(folderBox, filterBox, numWordsLabel, inputNumWords);

        /*--------- APP CONTROLS ---------*/

        final FlowPane appControlPane = new FlowPane(15, 15);
        appControlPane.setAlignment(Pos.CENTER);

        start.setPrefWidth(80);
        stop.setPrefWidth(80);
        resume.setPrefWidth(80);
        start.setDisable(true);
        stop.setDisable(true);
        resume.setDisable(true);

        appControlPane.getChildren().addAll(start, stop, resume);

        final BorderPane panel = new BorderPane();
        panel.setLeft(parametersPane);
        panel.setRight(appControlPane);

        /*--------- DISPLAY ---------*/

        FlowPane display = new FlowPane(15, 15);
        textArea.setPrefColumnCount(69);
        textArea.setPrefRowCount(18);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setFont(Font.font(13));

        ScrollPane scrollPane = new ScrollPane(textArea);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);

        display.getChildren().add(scrollPane);


        /*------------------- ROOT MANAGEMENT -----------------*/

        root.getChildren().addAll(panel, display);
        for (Node n : root.getChildren()) {
            FlowPane.setMargin(n, new Insets(20));
        }
        stage.setScene(new Scene(root, 920, 530));
        stage.show();

        stage.setOnCloseRequest(event -> System.exit(0));


        /* -------------------- ACTIONS ----------------------*/
        openFolder.setOnAction(e -> {
            final File selectedDirectory = directoryChooser.showDialog(stage);
            if (selectedDirectory != null) {
                folderPath.setText(selectedDirectory.getAbsolutePath());
                start.setDisable(false);
            }
        });

        openFilterFile.setOnAction(e -> {
            final File selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                filterPath.setText(selectedFile.getAbsolutePath());
            }
        });

        start.setOnAction(e -> {
            textArea.clear();
            if (filterPath.getText().isEmpty())
                textArea.appendText("The program won't filter any words because no filter file was chosen.\n");

            start.setDisable(true);
            stop.setDisable(false);
            resume.setDisable(true);
            inputNumWords.setDisable(true);
            openFolder.setDisable(true);
            openFilterFile.setDisable(true);
            textArea.appendText("Processing...\n");
            String finalPath = filterPath.getText().isEmpty() ? null : filterPath.getText();
            new Thread(() -> controller.launch(folderPath.getText(), finalPath)).start();
        });

        stop.setOnAction(e -> {
            controller.pause();
            stop.setDisable(true);
            resume.setDisable(false);
        });

        resume.setOnAction(e -> {
            controller.resume();
            stop.setDisable(false);
            resume.setDisable(true);
        });

    }



    public void updateView(final Output output) {
        Platform.runLater(() -> {
            this.textArea.clear();
            this.textArea.appendText("NUMBER OF PROCESSED WORDS: " + output.getWordCount() + "\n");
            this.textArea.appendText("------------------------------------------------------\n");
            this.textArea.appendText("TOP " + this.inputNumWords.getText() + " WORDS:\n");
            output.getWordFrequency().entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(Integer.parseInt(inputNumWords.getText()))
                    .forEach(e -> this.textArea.appendText("{" + e.getKey() + ": " + e.getValue() + "}\n"));
        });
    }

    public void updateTime(final long time) {
        start.setDisable(false);
        stop.setDisable(true);
        resume.setDisable(true);
        inputNumWords.setDisable(false);
        openFolder.setDisable(false);
        openFilterFile.setDisable(false);
        Platform.runLater(() -> {
            this.textArea.appendText("------------------------------------------------------\n");
            this.textArea.appendText("ELAPSED TIME: " + time + "ms\n");
        });
    }

    private UnaryOperator<TextFormatter.Change> getFilter() {

        return t -> {

            if (t.isReplaced())
                if(t.getText().matches("[^0-9]"))
                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));


            if (t.isAdded()) {
                if (t.getControlText().contains(".")) {
                    if (t.getText().matches("[^0-9]")) {
                        t.setText("");
                    }
                } else if (t.getText().matches("[^0-9.]")) {
                    t.setText("");
                }
            }

            return t;
        };
    }
}
