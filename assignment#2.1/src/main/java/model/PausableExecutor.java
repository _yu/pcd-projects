package model;

import controller.ServiceListener;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PausableExecutor extends ThreadPoolExecutor {

    private boolean isPaused = false;
    private final Lock pauseLock = new ReentrantLock();
    private final Condition unpaused = pauseLock.newCondition();
    private ServiceListener listener;

    public PausableExecutor(final int sizePool) {
        super(sizePool, sizePool, 1, TimeUnit.DAYS, new LinkedBlockingQueue<>());
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        pauseLock.lock();
        try {
            while (isPaused) {
                unpaused.await();
            }
        } catch (InterruptedException e) {
            t.interrupt();
        } finally {
            pauseLock.unlock();
        }
    }

    @Override
    protected void terminated() {
        super.terminated();
        listener.notifyTermination();
    }

    public void pause() {
        pauseLock.lock();
        try {
            isPaused = true;
        } finally {
            pauseLock.unlock();
        }
    }

    public void resume() {
        pauseLock.lock();
        try {
            isPaused = false;
            unpaused.signalAll();
        } finally {
            pauseLock.unlock();
        }
    }

    public void addListener(ServiceListener listener){
        this.listener = listener;
    }

    public boolean isPaused() {
        return this.isPaused;
    }
}
