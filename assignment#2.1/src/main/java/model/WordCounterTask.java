package model;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;
import utils.Output;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * A class that represent the task of loading a file and count its words and occurrences.
 */
public class WordCounterTask implements Callable<Output> {

    private final List<String> blacklistedWords;
    private final File file;
    private final boolean debug;

    public WordCounterTask(final List<String> blacklistedWords, final File file, final boolean debug) {
        this.blacklistedWords = blacklistedWords;
        this.file = file;
        this.debug = debug;
    }

    @Override
    public Output call(){
        if (debug) log("start task");
        try {
            PDDocument document = PDDocument.load(file);
            AccessPermission ap = document.getCurrentAccessPermission();
            if (!ap.canExtractContent())
                throw new IOException("You do not have permission to extract text");
            PDFTextStripper stripper = new PDFTextStripper();
            String text = stripper.getText(document);
            document.close();
            return generateOutput(text);
        } catch (IOException e) { e.printStackTrace(); }
        return null;
    }

    private Output generateOutput(String text){
        Map<String, Integer> wordFrequency = new LinkedHashMap<>();

        final String[] words = text.replaceAll("[^\\p{L} ]", "").toLowerCase(Locale.ROOT).split("\\s+");
        Arrays.stream(words)
                .filter(w -> !blacklistedWords.contains(w))
                .forEach(w -> {
                    if (wordFrequency.putIfAbsent(w, 1) != null)
                        wordFrequency.put(w, wordFrequency.get(w) + 1);
                });

        if (debug) log("task ending");
        return new Output(words.length, wordFrequency);
    }

    private void log(final String msg) {
        System.out.println(Thread.currentThread() + msg);
    }
}
