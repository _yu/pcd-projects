package controller;

import model.PausableExecutor;
import model.WordCounterTask;
import utils.Output;
import view.ViewFX;
import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

public class WordCounterService {

    private final Output output;
    private final PausableExecutor executor;
    private final Queue<File> filesQueue;
    private final List<String> blacklistedWords;
    private final ViewFX view;

    public WordCounterService (final int poolSize,
                               final Collection<File> files,
                               final List<String> blacklistedWords,
                               final ViewFX view){
        this.executor = new PausableExecutor(poolSize);
        this.filesQueue = new ArrayDeque<>(files);
        this.blacklistedWords = blacklistedWords;
        this.view = view;

        this.output = new Output(0, new LinkedHashMap<>());
    }

    public void compute() {
        while (!filesQueue.isEmpty()) {
            final File f = filesQueue.poll();
            CompletableFuture.supplyAsync(() ->
                    new WordCounterTask(this.blacklistedWords, f, false).call(), executor)
                    .thenApply(res -> {
                        update(res);
                        if (!this.executor.isPaused()) view.updateView(output.getCopy());
                        return res;
                    });
        }

        executor.shutdown();
    }

    public void pause() {
        this.executor.pause();
    }

    public void resume() {
        this.executor.resume();
    }

    public void addListener(ServiceListener listener){
        this.executor.addListener(listener);
    }

    private void update(Output newOutput){
        this.output.updateWordCount(newOutput.getWordCount());
        newOutput.getWordFrequency().forEach((word, num) -> this.output.getWordFrequency()
                .put(word, num + this.output.getWordFrequency().getOrDefault(word,  0)));
    }

}
