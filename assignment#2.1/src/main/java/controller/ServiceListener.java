package controller;

public interface ServiceListener {

    /**
     * Called by WordCounterService to signal termination of
     * all reading tasks.
     */
    void notifyTermination();

}
