package controller;

import org.apache.commons.lang3.time.StopWatch;
import utils.UtilityFunctions;
import view.ViewFX;

import java.io.File;
import java.util.List;

public class ControllerImpl implements Controller, ServiceListener{

    private final static int NUM_THREAD = Runtime.getRuntime().availableProcessors() + 2;
    private final StopWatch watch;
    private final ViewFX view;
    private  WordCounterService service;

    public ControllerImpl(final ViewFX view) {
        this.view = view;
        this.watch = new StopWatch();
    }

    @Override
    public void launch(final String directoryPath, final String blacklistPath) {
        final List<String> blacklistedWords = UtilityFunctions.getFilterWords(blacklistPath == null ? null : new File(blacklistPath));
        final List<File> files = UtilityFunctions.getOrderedFiles(new File(directoryPath));
        service = new WordCounterService(NUM_THREAD, files, blacklistedWords, view);
        service.addListener(this);
        watch.reset();
        watch.start();
        service.compute();
    }

    @Override
    public synchronized void resume() {
        watch.resume();
        service.resume();
    }

    @Override
    public synchronized void pause() {
        service.pause();
        watch.suspend();
    }

    @Override
    public void notifyTermination() {
        watch.stop();
        view.updateTime(watch.getTime());
    }
}
