package puzzle;

public class SelectionManager {

	private boolean selectionActive = false;
	private Tile selectedTile;

	public void selectTile(final Tile tile, final Listener listener) {
		
		if(selectionActive) {
			selectionActive = false;
			
			//swap(selectedTile, tile);
			
			listener.onSwapPerformed(selectedTile.getId(), tile.getId());
		} else {
			selectionActive = true;
			selectedTile = tile;
		}

		System.out.println("SELECT TILE " + selectionActive);
	}

	public void swap(final Tile t1, final Tile t2) {
		int pos = t1.getCurrentPosition();
		t1.setCurrentPosition(t2.getCurrentPosition());
		t2.setCurrentPosition(pos);
	}
	
	@FunctionalInterface
	interface Listener{
		void onSwapPerformed(int id1, int id2);
	}
}
