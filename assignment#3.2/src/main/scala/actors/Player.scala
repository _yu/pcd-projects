package actors

import akka.actor.typed.receptionist.Receptionist
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, StashBuffer}
import app.Application.PlayerServiceKey
import app.{Application, PuzzleListener}
import puzzle.PuzzleBoard

import scala.collection.mutable.ListBuffer

object Player {
  sealed trait PlayerMessage extends CborSerializable

  //Play message
  final case object SetUp extends PlayerMessage
  final case object Start extends PlayerMessage
  final case class SendMove(id1: Int, id2: Int, timestamp:Long) extends PlayerMessage
  final case class ReceiveMove(id1: Int, id2: Int, timestamp:Long) extends PlayerMessage
  final case object ExecuteMove extends PlayerMessage

  //Join message
  final case object Join extends PlayerMessage
  final case class FindAnyPlayer(listing: Receptionist.Listing) extends PlayerMessage
  final case class SendTiles(newPlayerRef: ActorRef[PlayerMessage]) extends PlayerMessage
  final case class ReceiveTiles(tiles: Map[Integer, Integer]) extends PlayerMessage

  //Utility
  final case class UpdateNumPlayer(numPlayer: Int) extends PlayerMessage
  final case object OnClose extends PlayerMessage
  final case class ResendMoves(id1:Int, id2:Int, timestamp:Long) extends PlayerMessage

  private var puzzle: Option[PuzzleBoard] = None

  def apply(name: String): Behavior[PlayerMessage] = {
    Behaviors.withStash(100) { buffer => {
      Behaviors.setup(context =>
        new Player(context, buffer).play(name))
      }
    }
  }

  private class Player(context: ActorContext[PlayerMessage], buffer: StashBuffer[PlayerMessage]) {

    def play(name: String): Behavior[PlayerMessage] = {
      val findAnyPlayerResponseAdapter: ActorRef[Receptionist.Listing] = context.messageAdapter[Receptionist.Listing](FindAnyPlayer)
      var onJoin = false
      var numPlayer = 1
      val swapTiles: ListBuffer[(Int, Int, Long)] = new ListBuffer[(Int, Int, Long)]

      Behaviors.receiveMessage {
        case SetUp =>
          val n = 3
          val m = 5
          val imagePath = "src/main/java/puzzle/bletchley-park-mansion.jpg"
          val swapListener = new PuzzleListener(context.self)
          puzzle = Option.apply(new PuzzleBoard(name, n, m, imagePath, swapListener))
          context.system.receptionist ! Receptionist.Register(PlayerServiceKey, context.self)
          println("SET UP")
          Behaviors.same
        case Start =>
          puzzle.get.setVisible(true)
          println("START")
          Behaviors.same
        case SendMove(id1, id2, timestamp) =>
          (0 until numPlayer).foreach {
            _ => {
              Application.router.get ! ReceiveMove(id1, id2, timestamp)
              println("SEND MOVE " + id1 + " " + id2 +s" with num players $numPlayer")
            }
          }
          Behaviors.same
        case ReceiveMove(id1, id2, timestamp) =>
          if (onJoin) {
            buffer.stash(ResendMoves(id1, id2, timestamp))
          } else {
            swapTiles.prepend((id1, id2, timestamp))
            println(s"MOVE $id1 - $id2 at $timestamp")
            context.self ! ExecuteMove
          }
          Behaviors.same
        case ExecuteMove =>
          swapTiles.sortBy(tile =>(tile._3, tile._1))
          if (System.currentTimeMillis() > swapTiles.last._3 + 150) {
            val tile = swapTiles.last
            puzzle.get.swapTiles(tile._1, tile._2)
            swapTiles.remove(swapTiles.size-1)
            if (swapTiles.nonEmpty) {
              val repeatingMoves = swapTiles.filter(t => ((t._1 == tile._1 && t._2 == tile._2) ||
                (t._1 == tile._2 && t._2 == tile._1)) && t._3 < tile._3+500)
              swapTiles --= repeatingMoves
            }
            if (swapTiles.nonEmpty)  context.self ! ExecuteMove
          } else {
            context.self ! ExecuteMove
          }
          Behaviors.same

        case Join =>
          println("ASK TO JOIN")
          context.system.receptionist ! Receptionist.Find(Application.PlayerServiceKey, findAnyPlayerResponseAdapter)
          Behaviors.same
        case FindAnyPlayer(Application.PlayerServiceKey.Listing(listings)) =>
          if (listings.isEmpty || listings.size < 2) {
            context.system.receptionist ! Receptionist.Find(Application.PlayerServiceKey, findAnyPlayerResponseAdapter)
          } else {
            println("ASK FOR TILES")
            val players = listings.filter(ref => ref != context.self)
            numPlayer += players.size
            players.foreach(ref => ref ! UpdateNumPlayer(1))
            onJoin = true
            players.head ! SendTiles(context.self)
          }
          Behaviors.same
        case SendTiles(playerRef) =>
          println("SEND TILES")
          var tiles: Map[Integer, Integer] = Map.empty
          puzzle.get.getCurrentTiles.forEach(t =>
            tiles = tiles + (Integer.valueOf(t.getId) -> Integer.valueOf(t.getCurrentPosition)))
          playerRef ! ReceiveTiles(tiles)
          Behaviors.same
        case ReceiveTiles(tiles) =>
          val map: java.util.Map[Integer, Integer] = new java.util.HashMap()
          tiles.foreach(v => map.put(v._1, v._2))
          puzzle.get.setTiles(map)
          puzzle.get.paintPuzzle()
          println("RECEIVE")
          buffer.unstashAll(Behaviors.same)
          onJoin = false
          context.self ! Start
          Behaviors.same

        case OnClose =>
          (0 until numPlayer).foreach{
            _ => Application.router.get ! UpdateNumPlayer(-1)
          }
          context.system.terminate()
          Behaviors.same
        case UpdateNumPlayer(num) =>
          numPlayer += num
          println(s"NOW NUM PLAYER IS $numPlayer")
          Behaviors.same
        case ResendMoves(id1, id2, timestamp) =>
          swapTiles.prepend((id1, id2, timestamp))
          context.self ! ExecuteMove
          Behaviors.same
      }
    }
  }
}
