package app

import actors.Player.{OnClose, PlayerMessage, SendMove}
import akka.actor.typed.ActorRef

class PuzzleListener(player: ActorRef[PlayerMessage]) {
  def notifySwap(id1: Int, id2: Int): Unit =
    player ! SendMove(id1, id2, System.currentTimeMillis)

  def notifyEnd(): Unit = {
    player ! OnClose
  }

}
