package app

import actors.Player
import actors.Player.PlayerMessage
import akka.actor.typed.receptionist.ServiceKey
import akka.actor.typed.scaladsl.{Behaviors, GroupRouter, Routers}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import com.typesafe.config.ConfigFactory

object Application {
  val PlayerServiceKey: ServiceKey[PlayerMessage] = ServiceKey[PlayerMessage]("PlayerService")
  val group: GroupRouter[PlayerMessage] = Routers.group(PlayerServiceKey)
  var router:Option[ActorRef[PlayerMessage]] = Option.empty

  object RootBehavior {

    def apply(toJoin:Boolean, name:String): Behavior[Nothing] = Behaviors.setup[Nothing] { context =>
      val actor = context.spawn(Player(name), "Player")
      val poolWithBroadcast = group.withRoundRobinRouting()
      router = Option.apply(context.spawn(poolWithBroadcast, "worker-group"))
      actor ! Player.SetUp
      if (toJoin) {
        actor ! Player.Join
      } else {
        actor ! Player.Start
      }
      Behaviors.empty
    }
  }

  //args are: username:String port:String join:true/false
  def main(args: Array[String]): Unit = {
    val port = args(1)
    val config = ConfigFactory.parseString(s"""
      akka.remote.artery.canonical.port=$port
      """).withFallback(ConfigFactory.load("application"))

    ActorSystem[Nothing](RootBehavior(args(2) == "true", args(0)), "ClusterSystem", config)
  }
}

//cd C:\Users\uwu\Desktop\uni\pcd\project\assignment#3.2
//sbt "runMain app.Application yuqi 25251 false"
//sbt "runMain app.Application elena 25252 true"